import React from 'react';
import Image1 from './assets/images/img.png';
import Image2 from './assets/images/img_1.png';
import Image3 from './assets/images/img_2.png';
import Image4 from './assets/images/img_3.png';
import Image5 from './assets/images/img_4.png';
import Image6 from './assets/images/img_5.png';
import IUserInfo from './interfaces/UserInfo';
import ICategory from './interfaces/Category';
import Banner from './components/Banner';
import Category from './components/Category';
import Footer from './components/Footer';
import './app.css';

function App() {
  const userInfo: IUserInfo = {
    coinRemaining: 340,
    coinPaid: 1200,
    targetPromotion: 2000,
  };

  const categories: Array<ICategory> = [
    {
      id: 'CAT0001',
      title: 'Petrol',
      cards: [
        {
          id: 'ID0001',
          imagePath: Image1,
          amount: 15,
          description: '50% discount for every $100 top-up on your Shell Petrol Card',
        },
        {
          id: 'ID0002',
          imagePath: Image2,
          amount: 1000,
          description: '70% discount top-up on your Shell Petrol Card',
        },
      ],
    },
    {
      id: 'CAT0002',
      title: 'Rental Rebate',
      cards: [
        {
          id: 'ID0003',
          imagePath: Image3,
          amount: 20,
          description: 'Get $20 Rental rebate',
        },
        {
          id: 'ID0004',
          imagePath: Image4,
          amount: 15,
          description: 'Get $500 Rental rebate',
        },
      ],
    },
    {
      id: 'CAT0003',
      title: 'Food and Beverage',
      cards: [
        {
          id: 'ID0005',
          imagePath: Image5,
          amount: 25,
          description: 'NTUC Fairprice $50 Voucher',
        },
        {
          id: 'ID0006',
          imagePath: Image6,
          amount: 5,
          description: 'Free Cold Stone Sundae at any flavour!',
        },
      ],
    },
  ];

  return (
    <div className="container">
      <Banner userInfo={userInfo} />

      <div className="content">
        {
          categories.map((category: ICategory) => category.cards.length > 0 && (
            <Category
              key={category.id}
              title={category.title}
              coinRemaining={userInfo.coinRemaining}
              cards={category.cards}
            />
          ))
        }
      </div>

      <Footer hasNotification />
    </div>
  );
}

export default App;
