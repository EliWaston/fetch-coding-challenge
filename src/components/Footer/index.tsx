import React from 'react';
import { ReactComponent as Home } from '../../assets/images/icons/home.svg';
import { ReactComponent as RedCircle } from '../../assets/images/icons/red-circle.svg';
import { ReactComponent as Notification } from '../../assets/images/icons/notification.svg';
import { ReactComponent as Card } from '../../assets/images/icons/card.svg';
import { ReactComponent as Person } from '../../assets/images/icons/person.svg';
import './footer.css';

interface FooterProps {
  hasNotification?: boolean;
}

const Footer = (props: FooterProps) => {
  return (
    <div className="footer-root">
      <div className="icon-wrapper">
        <button type="button" className="icon-button">
          <Home />
        </button>
        <button type="button" className="icon-button notification-wrapper">
          <Notification />
          {
            props.hasNotification && <RedCircle className="red-circle" />
          }
        </button>
        <button type="button" className="icon-button">
          <Card />
        </button>
        <button type="button" className="icon-button">
          <Person />
        </button>
      </div>
      <div className="dash" />
    </div>
  );
};

export default Footer;
