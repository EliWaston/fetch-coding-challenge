import React from 'react';
import IUserInfo from '../../../interfaces/UserInfo';
import { formatNumber } from '../../../utils/helper';
import ProgressBar from '../../ProgressBar';
import './coupons.css';

interface CouponsProps {
  userInfo: IUserInfo;
}

const Coupons = (props: CouponsProps) => {
  return (
    <div className="coupons-root">
      <p className="available-coin">Available Coin balance</p>
      <p className="coin-balance-amount">{formatNumber(props.userInfo.coinRemaining)}</p>
      <ProgressBar
        backgroundColor="#0062ff"
        progress={props.userInfo.coinPaid / props.userInfo.targetPromotion * 100}
      />
      <p className="tier-description">
        {`You have paid rental fee for $${formatNumber(props.userInfo.coinPaid)}. Pay more $${formatNumber(props.userInfo.targetPromotion - props.userInfo.coinPaid)} to achieve Gold Tier.`}
      </p>
      <a href="/tier-benefits" className="tier-link">View tier benefits <b>&gt;</b></a>
      <div className="my-coupon-wrapper">
        <button className="my-coupon-btn">My Coupons</button>
        <p className="updated-date">Updated: 02/11/2021</p>
      </div>
    </div>
  );
};

export default Coupons
