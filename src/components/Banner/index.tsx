import React from 'react';
import { ReactComponent as Arrow } from '../../assets/images/icons/arrow.svg';
import IUserInfo from '../../interfaces/UserInfo';
import Coupons from './Coupons';
import './banner.css';

interface BannerProps {
  userInfo: IUserInfo;
}

const Banner = (props: BannerProps) => {
  return (
    <div className="banner-root">
      <div className="banner-content-wrapper">
        <button type="button" className="icon-button">
          <Arrow />
        </button>
        <h3 className="banner-title">Silver Tier</h3>
        <p className="banner-description">
          In Silver Tier, every $1 in rental fee paid, you get 2 coins to redeem exclusive rewards.
        </p>
      </div>

      <Coupons userInfo={props.userInfo} />
    </div>
  );
};

export default Banner;
