import React from 'react';
import './progressBar.css';

interface ProgressBarProps {
  backgroundColor: string;
  progress: number;
}

const ProgressBar = (props: ProgressBarProps) => {
  return (
    <div className="progress-bar-root">
      <div
        className="progress-bar-percent"
        style={{
          backgroundColor: props.backgroundColor,
          width: `${props.progress}%`,
        }}
      />
    </div>
  );
};

export default ProgressBar;
