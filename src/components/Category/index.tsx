import React from 'react';
import ICard from '../../interfaces/Card';
import CardItem from './CardItem';
import './category.css';

interface CardListProps {
  title: string;
  coinRemaining: number;
  cards: Array<ICard>;
}

const Category = (props: CardListProps) => {
  return (
    <div className="category-root">
      <p className="category-title">{props.title}</p>

      <div className="category-list">
        {
          props.cards.map((card: ICard) => (
            <CardItem
              key={card.id}
              coinRemaining={props.coinRemaining}
              card={card}
            />
          ))
        }
      </div>
    </div>
  );
};

export default Category;
