import React from 'react';
import { ReactComponent as InvalidIcon } from '../../../assets/images/icons/invalid-coin.svg';
import ICard from '../../../interfaces/Card';
import { formatNumber } from '../../../utils/helper';
import './cardItem.css';

interface CardItemProps {
  coinRemaining: number;
  card: ICard;
}

const CardItem = (props: CardItemProps) => {
  const isValid: boolean = props.card.amount <= props.coinRemaining;

  return (
    <div className="card-item-root">
      <img
        alt={props.card.id}
        src={props.card.imagePath}
        className="card-item-image"
      />

      <div className="card-item-content-wrapper">
        {
          isValid ? (
            <p className="card-item-amount">{`${formatNumber(props.card.amount)} Coins`}</p>
          ) : (
            <p className="card-item-amount invalid">
              <InvalidIcon />
              {` ${formatNumber(props.card.amount)} Coins`}
            </p>
          )
        }
        <p className="card-item-description text-ellipsis">{props.card.description}</p>
        {
          !isValid && (
            <p className="insufficient-coins">Insufficient coins</p>
          )
        }
      </div>
    </div>
  );
}

export default CardItem;
