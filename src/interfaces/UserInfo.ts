export default interface UserInfo {
  coinRemaining: number;
  coinPaid: number;
  targetPromotion: number;
}
