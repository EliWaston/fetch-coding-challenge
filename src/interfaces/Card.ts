export default interface Card {
  id: string;
  imagePath: string;
  amount: number;
  description: string;
}
