import ICard from './Card';

export default interface Category {
  id: string;
  title: string;
  cards: Array<ICard>;
}
