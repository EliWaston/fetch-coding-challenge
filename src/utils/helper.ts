export const formatNumber = (number: number, digits: number = 2) => {
  const x = number.toString().split('.');
  let x1 = x[0];
  const x2 = x.length > 1 ? `.${x[1].substring(0, digits)}` : '';
  const rgx = /(\d+)(\d{3})/;

  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1,$2');
  }

  return x1 + x2;
};
